import { useState } from "react";
import store from "./reduxenv/store";
import Adddepartment from "./components/department_modal";
import DepartmentTable from "./components/department_table";

function Department() {
    const [departments, updatedepartments] = useState(
        store.getState().departments
    );
    store.subscribe(() => updatedepartments(store.getState().departments));

    const [showdepform, setshowdepform] = useState(false);
    const [createnewdep, setcreatenewdep] = useState(true);
    const [depname, setdepname] = useState("");

    function toggleformstate() {
        setshowdepform(!showdepform);
    }

    const [departid, setdepartid] = useState(
        Math.max.apply(null, [...departments.map((u) => u.id)]) + 1
    );
    function createnewdepartment() {
        store.dispatch({
            type: createnewdep ? "createDepartment" : "updateDepartment",
            payload: {
                id: departid,
                name: document.getElementById("depname").value,
            },
        });
        setdepartid(
            Math.max.apply(null, [...departments.map((u) => u.id)]) + 1
        );
        setdepname("");
        toggleformstate();
    }
    function deleteDepartment(id) {
        store.dispatch({
            type: "deleteDepartment",
            payload: {
                id: id,
            },
        });
    }
    return (
        <div>
            {showdepform ? (
                <Adddepartment
                    showdepform={showdepform}
                    toggleformstate={toggleformstate}
                    createnewdep={createnewdep}
                    depname={depname}
                    createnewdepartment={createnewdepartment}
                />
            ) : null}
            <DepartmentTable
                toggleformstate={toggleformstate}
                departments={departments}
                setcreatenewdep={setcreatenewdep}
                setdepartid={setdepartid}
                setdepname={setdepname}
                deleteDepartment={deleteDepartment}
            />
        </div>
    );
}

export default Department;
