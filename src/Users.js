import { useState } from "react";
import UserForm from "./components/user_modal";
import Users_table from "./components/user_table";
import store from "./reduxenv/store";

function Users() {
    const [users, updateusers] = useState(store.getState().users);
    const [departments, updatedepartments] = useState(
        store.getState().departments
    );
    store.subscribe(() => updateusers(store.getState().users));
    store.subscribe(() => updatedepartments(store.getState().departments));

    const [isemail, setisemail] = useState(false);
    const [isstring, setisstring] = useState(false);

    const [showuserform, setuserform] = useState(false);

    const [useridvalue, setuseridvalue] = useState(
        Math.max.apply(null, [...users.map((u) => u.id)]) + 1
    );
    const [usernamevale, setusernamevale] = useState("");
    const [emailvale, setemailvale] = useState("");
    const [departmentvale, setdepartmentvale] = useState(false);

    const toggleformstate = () => setuserform(!showuserform);

    const [createuser, setcreateuser] = useState(true);

    function createnewuser() {
        store.dispatch({
            type: createuser ? "createUser" : "updateUser",
            payload: {
                id: useridvalue,
                name: document.getElementById("username").value,
                email: document.getElementById("email").value,
                department: departmentvale,
            },
        });
        toggleformstate();
    }
    function deleteuser(id) {
        store.dispatch({
            type: "deleteUser",
            payload: {
                id: id,
            },
        });
    }

    function useractions(
        id = Math.max.apply(null, [...users.map((u) => u.id)]) + 1,
        name = "",
        email = "",
        department = departments[0].id
    ) {
        setuseridvalue(id);
        setusernamevale(name);
        setemailvale(email);
        setdepartmentvale(department);
        toggleformstate();
    }

    return (
        <div>
            <UserForm
                showuserform={showuserform}
                toggleformstate={toggleformstate}
                createuser={createuser}
                usernamevale={usernamevale}
                useridvalue={useridvalue}
                emailvale={emailvale}
                departmentvale={departmentvale}
                setdepartmentvale={setdepartmentvale}
                setusernamevale={setusernamevale}
                setemailvale={setemailvale}
                departments={departments}
                createnewuser={createnewuser}
                isemail={isemail}
                setisemail={setisemail}
                isstring={isstring}
                setisstring={setisstring}
            />

            <Users_table
                setcreateuser={setcreateuser}
                useractions={useractions}
                users={users}
                departments={departments}
                deleteuser={deleteuser}
                setisemail={setisemail}
                setisstring={setisstring}
            />
        </div>
    );
}

export default Users;
