import datas from "../data.json";

function reducer(state = datas, action) {
    if (action.type === "createUser") {
        return {
            users: [
                ...state.users,
                {
                    id: action.payload.id,
                    username: action.payload.name,
                    email: action.payload.email,
                    department: action.payload.department,
                },
            ],
            departments: [...state.departments],
        };
    } else if (action.type === "deleteUser") {
        return {
            users: [...state.users.filter((u) => u.id !== action.payload.id)],
            departments: [...state.departments],
        };
    } else if (action.type === "updateUser") {
        return {
            users: [
                ...state.users.filter((u) => u.id !== action.payload.id),
                {
                    id: action.payload.id,
                    username: action.payload.name,
                    email: action.payload.email,
                    department: action.payload.department,
                },
            ],
            departments: [...state.departments],
        };
    } else if (action.type === "createDepartment") {
        return {
            departments: [
                ...state.departments,
                {
                    id: action.payload.id,
                    name: action.payload.name,
                },
            ],
            users: [...state.users],
        };
    } else if (action.type === "deleteDepartment") {
        return {
            departments: [
                ...state.departments.filter((d) => d.id !== action.payload.id),
            ],
            users: [...state.users],
        };
    } else if (action.type === "updateDepartment") {
        return {
            departments: [
                ...state.departments.filter((d) => d.id !== action.payload.id),
                {
                    id: action.payload.id,
                    name: action.payload.name,
                },
            ],
            users: [...state.users],
        };
    } else {
        return state;
    }
}

export default reducer;
