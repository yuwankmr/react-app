import * as React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
const itemData = [...Array(24).keys()].map(
    (n) => "https://source.unsplash.com/random/200x200?sig=" + n
);

export default function StandardImageList() {
    return (
        <ImageList sx={{ width: "80vw", m: "auto" }} cols={3}>
            {itemData.map((item) => (
                <ImageListItem key={item}>
                    <img
                        src={`${item}`}
                        srcSet={`${item}`}
                        alt={item}
                        loading="lazy"
                    />
                </ImageListItem>
            ))}
        </ImageList>
    );
}
