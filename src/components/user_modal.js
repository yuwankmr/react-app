import { Button, Modal } from "@mui/material";
import { Box, MenuItem, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
var validator = require("validator");

export default function UserForm(props) {
    return (
        <Modal
            open={props.showuserform}
            onClose={props.toggleformstate}
            sx={{ backdropFilter: "blur(3px)" }}
            aria-labelledby="Create User Form"
            aria-describedby="Create New user"
        >
            <Box
                component="form"
                sx={{
                    m: "auto",
                    mt: 10,
                    Width: "100%",
                    maxWidth: "500px",
                    bgcolor: "background.paper",
                    borderRadius: 2,
                }}
                noValidate
                autoComplete="off"
            >
                <br></br>
                <Typography align="center" variant="h4" sx={{ m: 1 }}>
                    {props.createuser
                        ? "Create New User"
                        : "Edit " + props.usernamevale}
                </Typography>
                <TextField
                    id="userid"
                    label="UserID"
                    variant="outlined"
                    required={true}
                    value={props.useridvalue}
                    disabled={true}
                    sx={{ m: 2 }}
                />
                <TextField
                    id="username"
                    label="Username"
                    variant="outlined"
                    color={props.isstring ? "primary" : "error"}
                    defaultValue={props.usernamevale}
                    onChange={(e) => {
                        props.setisstring(!validator.isEmpty(e.target.value));
                    }}
                    sx={{ m: 2 }}
                />
                <TextField
                    id="email"
                    label="Email"
                    variant="outlined"
                    defaultValue={props.emailvale}
                    color={props.isemail ? "primary" : "error"}
                    onChange={(e) => {
                        props.setisemail(validator.isEmail(e.target.value));
                    }}
                    sx={{ m: 2 }}
                />
                <TextField
                    id="department"
                    select
                    label="Department"
                    defaultValue={props.departmentvale}
                    sx={{ m: 2, width: "max-content" }}
                    onChange={(e) => {
                        props.setdepartmentvale(e.target.value);
                    }}
                >
                    {props.departments.map((option) => (
                        <MenuItem key={option.id} value={option.id}>
                            {option.name}
                        </MenuItem>
                    ))}
                </TextField>

                <Button
                    variant="contained"
                    sx={{ p: 2, m: 2, width: "calc(100% - 40px)" }}
                    onClick={props.createnewuser}
                    disabled={!(props.isemail & props.isstring)}
                >
                    SAVE
                </Button>
                <Button
                    onClick={props.toggleformstate}
                    variant="contained"
                    color="error"
                    sx={{ p: 2, m: 2, mt: 0, width: "calc(100% - 40px)" }}
                >
                    Cancel
                </Button>
            </Box>
        </Modal>
    );
}
