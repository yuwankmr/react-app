import { Button, Modal, Box, TextField, Typography } from "@mui/material";
import { useState } from "react";
var validator = require("validator");

export default function Adddepartment(props) {
    const [isstring, setisstring] = useState(!validator.isEmpty(props.depname));
    return (
        <Modal
            open={props.showdepform}
            onClose={props.toggleformstate}
            sx={{ backdropFilter: "blur(3px)" }}
            aria-labelledby="Create User Form"
            aria-describedby="Create New user"
        >
            <Box
                component="form"
                sx={{
                    m: "auto",
                    mt: 10,
                    Width: "100%",
                    maxWidth: "500px",
                    bgcolor: "background.paper",
                    borderRadius: 2,
                }}
                noValidate
                autoComplete="off"
            >
                <br></br>
                <Typography
                    component={"h1"}
                    align="center"
                    variant="h4"
                    sx={{ m: 1 }}
                >
                    {props.createnewdep
                        ? "Create New Department"
                        : "Edit " + props.depname}
                </Typography>
                <TextField
                    id="depname"
                    label="Department name"
                    variant="outlined"
                    color={isstring ? "primary" : "error"}
                    defaultValue={props.depname}
                    onChange={(e) => {
                        setisstring(!validator.isEmpty(e.target.value));
                    }}
                    sx={{ m: 2, width: "calc(100% - 40px)" }}
                />
                <Button
                    variant="contained"
                    disabled={!isstring}
                    onClick={props.createnewdepartment}
                    sx={{ p: 2, m: 2, width: "calc(100% - 40px)" }}
                >
                    SAVE
                </Button>
                <Button
                    onClick={props.toggleformstate}
                    variant="contained"
                    color="error"
                    sx={{ p: 2, m: 2, mt: 0, width: "calc(100% - 40px)" }}
                >
                    Cancel
                </Button>
            </Box>
        </Modal>
    );
}
