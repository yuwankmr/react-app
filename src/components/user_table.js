import { Add, Delete, Edit } from "@mui/icons-material";
import {
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
} from "@mui/material/";

export default function Users_table(props) {
    return (
        <TableContainer component={Paper}>
            <Button
                sx={{
                    p: [0, 3],
                    borderRadius: "50%",
                    position: "fixed",
                    bottom: 32,
                    right: 32,
                }}
                variant="contained"
                onClick={() => {
                    props.setcreateuser(true);
                    props.useractions();
                }}
            >
                <Add />
            </Button>
            <Table
                // sx={{ minWidth: 650 }}
                size="small"
                aria-label="Departments Table"
            >
                <TableHead>
                    <TableRow>
                        <TableCell>ID</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Department</TableCell>
                        <TableCell align="center">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {[]
                        .concat(props.users)
                        .sort((a, b) => (a.id > b.id ? 1 : -1))
                        .map((user) => (
                            <TableRow key={user.id}>
                                <TableCell>U{user.id}</TableCell>
                                <TableCell>{user.username}</TableCell>
                                <TableCell>{user.email}</TableCell>
                                <TableCell>
                                    {props.departments.find(
                                        (d) => d.id === user.department
                                    ) !== undefined
                                        ? props.departments.find(
                                              (d) => d.id === user.department
                                          ).name
                                        : "Department Deleted"}
                                </TableCell>
                                <TableCell align="center">
                                    <Button
                                        color="error"
                                        onClick={() =>
                                            props.deleteuser(user.id)
                                        }
                                    >
                                        <Delete />
                                    </Button>
                                    <Button>
                                        <Edit
                                            onClick={() => {
                                                props.setcreateuser(false);
                                                props.setisemail(true);
                                                props.setisstring(true);
                                                props.useractions(
                                                    user.id,
                                                    user.username,
                                                    user.email,
                                                    user.department
                                                );
                                            }}
                                        />
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
