import { Delete, Edit, Add } from "@mui/icons-material";
import {
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@mui/material";
import Paper from "@mui/material/Paper";

export default function DepartmentTable(props) {
    return (
        <TableContainer component={Paper}>
            <Button
                sx={{
                    p: [0, 3],
                    borderRadius: "100%",
                    position: "fixed",
                    bottom: 32,
                    right: 32,
                }}
                variant="contained"
                onClick={() => {
                    props.toggleformstate();
                    props.setdepname("");
                }}
            >
                <Add />
            </Button>
            <Table
                sx={{ minWidth: 650 }}
                size="small"
                aria-label="a dense table"
            >
                <TableHead>
                    <TableRow>
                        <TableCell>ID</TableCell>
                        <TableCell>Department Name</TableCell>
                        <TableCell align="center">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {[]
                        .concat(props.departments)
                        .sort((a, b) => (a.id > b.id ? 1 : -1))
                        .map((department) => (
                            <TableRow key={department.id}>
                                <TableCell>D{department.id}</TableCell>
                                <TableCell>{department.name}</TableCell>
                                <TableCell align="center">
                                    <Button color="error">
                                        <Delete
                                            onClick={() =>
                                                props.deleteDepartment(
                                                    department.id
                                                )
                                            }
                                        />
                                    </Button>
                                    <Button>
                                        <Edit
                                            onClick={() => {
                                                props.setcreatenewdep(false);
                                                props.setdepartid(
                                                    department.id
                                                );
                                                props.setdepname(
                                                    department.name
                                                );
                                                props.toggleformstate();
                                            }}
                                        />
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
