import React, { useState } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import DocumentMeta from "react-document-meta";

ReactDOM.render(
    <React.StrictMode>
        <DocumentMeta
            {...{
                meta: {
                    name: "viewport",
                    content: "width=device-width, initial-scale=1",
                },
            }}
        />
        <App />
    </React.StrictMode>,
    document.getElementById("root")
);
